// Description:
//    Simple script that allows for listing, starting and stopping playlists
//    running on a Falcon Pi Player instance.
// 
// Dependencies:
//   hubot-rocketchat
//
// Commands:
//    bot help - Lists available commands
//    bot get playlists - Reply with list of playlists available
//    bot start <playlist> - Start named playlist immediately
//    bot stop - Stop current playlist
//    bot debug - Replys with information on all network interfaces
//
const parseString = require('xml2js').parseString;

const os = require( 'os' );

module.exports = (robot) => {
  const fppUrl = process.env.FPP_URL || 'http://localhost';

  function handleError(err, httpResponse, botResponse) {
    botResponse.reply('Beklager! Noe gikk galt.');
    robot.adapter.sendDirect({ user: { name: "hosh" } }, `Fix yo shit n00b! err: ${err} httpResponse: ${httpResponse}`);
  }

  function stopNow(botResponse, successCallback) {
    robot.http(`${fppUrl}/fppxml.php?command=stopNow`)
      .get()(function(err, httpResponse, body) {
        if (err) {
          handleError(err, httpResponse, botResponse);
        } else if (successCallback) {
          successCallback();
        }
      });
  }

  function correctPlaylistNameCase(botResponse, desiredPlaylistName, successCallback) {
    getPlaylists(botResponse, function(playlistNames) {
      const playlistMap = playlistNames.reduce(function(map, item){
        map[item.toLowerCase()] = item;
        return map;
      }, {});
      const nameWithCorrectCase = playlistMap[desiredPlaylistName.toLowerCase()];
      if (!nameWithCorrectCase) {
        botResponse.reply(`Klart ikke å finne playlist med navn ${desiredPlaylistName}`);
      } else {
        successCallback(nameWithCorrectCase);
      }
    });
  }

  function startPlaylist(botResponse, playlistName) {
    correctPlaylistNameCase(botResponse, playlistName, function(nameWithCorrectCase) {
      // Need to stop currently playing playlist otherwise start command freezes
      // the fpp daemon, but we don't do that until we know that we have a valid name
      stopNow(botResponse, function() {
          _startPlaylist(botResponse, nameWithCorrectCase);
      });
    });
  }

  function _startPlaylist(botResponse, playlistName) {
    robot.http(`${fppUrl}/fppxml.php?command=startPlaylist&playList=${playlistName}&repeat=checked`)
      .get()(function(err, httpResponse, body){
        if (err) {
          handleError(err, httpResponse, botResponse);
        }
      });
  }

  function getPlaylists(botResponse, successCallback) {
    robot.http(`${fppUrl}/fppxml.php?command=getPlayLists`)
      .get()(function(err, httpResponse, body) {
        if (err) {
          handleError(err, httpResponse, botResponse);
        } else {
          parseString(body, function (err, result) {
            successCallback(result.Playlists.Playlist);
          });
        }
    });
  }

  robot.respond(/\bhelp\b/i, function(botResponse) {
    botResponse.reply(`Jeg svarer på følgende kommandoer;
    * \`help\`: Jeg svarer med denne behelpelig forklaring på hva jeg kan gjøre for deg.
    * \`get playlists\`: Jeg svarer med en liste over tilgjengelig lysmønster
    * \`start <playlistName>\`: Jeg skrur på lys etter ønsket mønster
    * \`stop\`: Juletreet blir skrudd av
    `);
  });

  robot.respond(/\bget playlists\b/i, function (botResponse) {
    getPlaylists(botResponse, function(playlistNames){
      const playlistNamesAsString = playlistNames.map((name) => `:christmas_tree: ${name}`).join('\n');
      botResponse.reply(`Du kan velge fra:\n ${playlistNamesAsString}`);
    });
  });

  robot.respond(/\bstart (?:playlist )?(\w+)/i, function(botResponse) {
    const playlistName = botResponse.match[1];
    startPlaylist(botResponse, playlistName);
  });

  robot.respond(/\bstop\b/i, function(botResponse) {
    stopNow(botResponse);
  });

  robot.respond(/\bdebug\b/i, function(botResponse){
    const networkInterfaces = os.networkInterfaces();
    botResponse.reply('```\n' + JSON.stringify(networkInterfaces, null, 2) + '```');
  });
}